package Storeservlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String address = request.getParameter("address");
        String city = request.getParameter("city");
        String province = request.getParameter("province");
        String cardname = request.getParameter( "cardname");
        String cardnumber = request.getParameter("cardnumber");
        String ccvnumber = request.getParameter("ccvnumber");
        String username  = request.getParameter("username");
        String password = request.getParameter("password");
        out.println("<p> First name: " + firstname);
        out.println("<p> Last name: " + lastname);
        out.println("<p> Card name"   + cardname);
        out.println("<p> Card number"   + cardnumber);
        out.println("<p> ccv number"   + ccvnumber);
        out.println("<h1>Super Secret Login Information</h1>");
        out.println("<p> Username: " + username + "</p>");
        out.println("<p>Password: " + password + "</p>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out =response.getWriter();
        out.println("This resource is not available directly.");
    }
}
